package nl.ced.service;
 
import java.sql.SQLException;
import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Singleton;
import nl.ced.model.Persoon;
import nl.ced.persistence.PersoonDAO;




/**
 *
 * @author Peter van Vliet
 */
@Singleton
public class UserService extends BaseService<Persoon> {
    private PersoonDAO daoPersoon;

    @Inject
    public UserService(PersoonDAO daoPersoon) {
        this.daoPersoon = daoPersoon;
    }

    public Collection<Persoon> getAll() {
        return daoPersoon.getAll();
    }

    public Persoon get(int id) {
        return requireResult(daoPersoon.get(id));
    }
    public int add(Persoon user) {
        return daoPersoon.createPersoon(user.getVoornaam(), user.getAchternaam(), user.getTussenvoegsel(),
                user.getEmail(),user.getWachtwoord(),user.getTelnr(), user.getRol());
    }
    
    public void updateInfo(Persoon user) {
        daoPersoon.updatePersoon(user.getId(), user.getVoornaam(), user.getAchternaam(),
                                    user.getTussenvoegsel(),user.getEmail(),user.getWachtwoord(), 
                                    user.getTelnr(),user.getRol());
    }
    public void update(Persoon authenticator, int id, Persoon user) {
        // Controleren of deze gebruiker wel bestaat
        Persoon oldUser = get(id);

        if (!authenticator.hasRole(1)) {
            // Vaststellen dat de geauthenticeerde gebruiker
            // zichzelf aan het aanpassen is
            assertSelf(authenticator, oldUser);
        }
    }
    
    public void delete(int id) {
        daoPersoon.removePersoon(id);
    }

    public void unremove(int id) {
        daoPersoon.unremovePersoon(id);
    }
 }
