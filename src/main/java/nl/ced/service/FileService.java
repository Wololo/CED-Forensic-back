package nl.ced.service;

import nl.ced.model.*;
import nl.ced.persistence.DossierDAO;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Peter van Vliet
 */
@Singleton
public class FileService extends BaseService<Dossier> {
    private DossierDAO daoDossier;

    @Inject
    public FileService(DossierDAO daoDossier) {
        this.daoDossier = daoDossier;
    }

    public Collection<DossierOpbouw> getOpbouw(int id){
        
        return daoDossier.getOpbouw(id);
    }
    public Collection<Dossier> getAll() {
        return daoDossier.getAll();
    }

    public Collection<Status> getStatus() {
        return daoDossier.getStates();
    }
    public Collection<Opdrachtgever> getClients() {
        return daoDossier.getClients();
    }
    public Dossier get(int id) {
        return daoDossier.get(id);
    }



    public int add(Dossier dossier) {
        return(daoDossier.createDossier(dossier));
    }

    public void createClient(Opdrachtgever opdrachtgever) {
        daoDossier.createClient(opdrachtgever);
    }

    public void archive(int id) {
        daoDossier.archive(id);
    }

    public void recover(int id) {
        daoDossier.recover(id);
    }

    public void setStatus(int id,int status) {
        daoDossier.setStatus(id, status);
    }

    public int addProperty(Afdeling afdeling) {

      return daoDossier.addProperty(afdeling);
    }
    public int addMobility(Afdeling afdeling) {
        return daoDossier.addMobility(afdeling);
    }
    public int addVitality(Afdeling afdeling) {
        return daoDossier.addVitality(afdeling);
    }

    public int update(Dossier dossier) {
        return daoDossier.updateDossier(dossier);
    }

    public int updateVitality(Afdeling afdeling) {
        return daoDossier.updateVitality(afdeling);
    }

    public int updateMobility(Afdeling afdeling) {
        return daoDossier.updateMobility(afdeling);
    }

    public int updateProperty(Afdeling afdeling) {
        return daoDossier.updateProperty(afdeling);
    }
    public int saveOpbouw(InputStream fileInputStream,FormDataContentDisposition contentDispositionHeader, DossierOpbouw opbouw){
        return daoDossier.saveOpbouw(fileInputStream, contentDispositionHeader, opbouw);
    }

    public File getFile(int id) {
        try {
            return daoDossier.getFileFromDB(id);
        } catch (IOException ex) {
            Logger.getLogger(FileService.class.getName()).log(Level.SEVERE, null, ex);
        }

            return null;
    }
    
    public String getFileName(int id) {
        return daoDossier.getFileName(id);
    }

    public int removeOpbouw(int id) {
        return daoDossier.removeOpbouw(id);
    }

    public Collection<SoortSchade> getSoortSchade(int id) {
        return daoDossier.getSoortSchadeList(id);
    }

    public Collection<Dossier> getManagement(ManagementInfo info) {
        return daoDossier.getManagementInfo(info);
    }

    public int addSchade(SoortSchade schade) {
        return daoDossier.addSchade(schade);
    }

    public int removeClient(int id) {
        return daoDossier.removeClient(id);
    }

    public Opdrachtgever getClient(int id) {
        return daoDossier.getClients(id);
    }

    public void updateClient(Opdrachtgever opdrachtgever) {
        daoDossier.updateClient(opdrachtgever);
    }

    public void updateSoortSchade(SoortSchade soortSchade) {
        daoDossier.updateSchade(soortSchade);
    }

    public SoortSchade getSoortSchadeSingle(int id) {
        return daoDossier.getSoortSchadeFromWeb(id);
    }

    public int deleteSoortSchade(int id) {
        return daoDossier.removeSoortSchade(id);
    }
}

