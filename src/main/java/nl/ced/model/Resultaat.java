/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import nl.ced.View;

/**
 *
 * @author Dylan
 */



public class Resultaat {
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private String resultaat;
    @JsonIgnore
    public Resultaat(int id, String resultaat){
     this.id = id;
     this.resultaat = resultaat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResultaat() {
        return resultaat;
    }

    public void setResultaat(String resultaat) {
        this.resultaat = resultaat;
    }
    
}
