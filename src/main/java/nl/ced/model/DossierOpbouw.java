
package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import nl.ced.View;

/**
 *
 * @author Dylan
 */



public class DossierOpbouw {
    @JsonView(View.Public.class)
    private int id;
    @JsonView(View.Public.class)
    private String naam;
    @JsonView(View.Public.class)
    private String extension;
    @JsonView(View.Public.class)
    private String beschrijving;
    @JsonView(View.Public.class)
    private String titel;
    @JsonView(View.Public.class)
    private String datum_toevoeging;
    @JsonIgnore
    public DossierOpbouw(int id, String naam, String extension, String beschrijving, String titel, String datum_toevoeging){
     this.id = id;
     this.naam = naam;
     this.extension = extension;
     this.beschrijving = beschrijving;
     this.titel = titel;
     this.datum_toevoeging = datum_toevoeging;
    }
    
    public DossierOpbouw(){}
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getBeschrijving() {
        return beschrijving;
    }

    public void setBeschrijving(String beschrijving) {
        this.beschrijving = beschrijving;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getDatum_toevoeging() {
        return datum_toevoeging;
    }

    public void setDatum_toevoeging(String datum_toevoeging) {
        this.datum_toevoeging = datum_toevoeging;
    }

    
}
