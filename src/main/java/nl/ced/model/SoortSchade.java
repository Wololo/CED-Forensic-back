/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import nl.ced.View;

/**
 *
 * @author Dylan
 */



public class SoortSchade {
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private int afdeling;
    
    @JsonView(View.Public.class)
    private String naam;
    
    @JsonIgnore
    public SoortSchade(int id, int afdeling, String naam){
     this.id = id;
     this.afdeling = afdeling;
     this.naam = naam;
    }
    public SoortSchade(){}
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(int afdeling) {
        this.afdeling = afdeling;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }


    
}
