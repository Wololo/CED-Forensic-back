/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import nl.ced.View;

import java.util.ArrayList;

/**
 *
 * @author Dylan
 */



public class ManagementInfo {
    @JsonView(View.Public.class)
    private ArrayList<String> opdrachtgevers;
    @JsonView(View.Public.class)
    private ArrayList<String> afdelingen;
    @JsonView(View.Public.class)
    private ArrayList<String> resultaten;
    @JsonView(View.Public.class)
    private ArrayList<String> soort_schades;
    @JsonView(View.Public.class)
    private String datum_start;
    @JsonView(View.Public.class)
    private String datum_eind;
    @JsonView(View.Public.class)
    private int kwalificatie;
    @JsonView(View.Public.class)
    private int bespaarBedrag;
    @JsonView(View.Public.class)
    private int factuurBedrag;
    @JsonView(View.Public.class)
    private int expertId;

    @JsonIgnore
    public ManagementInfo(ArrayList<String> opdrachtgevers, ArrayList<String> afdelingen, ArrayList<String> resultaten,
                          ArrayList<String> soort_schades, String datum_start, String datum_eind, int kwalificatie,
                          int bespaarBedrag, int factuurBedrag, int expertId) {
        this.opdrachtgevers = opdrachtgevers;
        this.afdelingen = afdelingen;
        this.resultaten = resultaten;
        this.soort_schades = soort_schades;
        this.datum_start = datum_start;
        this.datum_eind = datum_eind;
        this.kwalificatie = kwalificatie;
        this.bespaarBedrag = bespaarBedrag;
        this.factuurBedrag = factuurBedrag;
        this.expertId = expertId;
    }
    public ManagementInfo(){}

    public ArrayList<String> getOpdrachtgevers() {
        return opdrachtgevers;
    }

    public void setOpdrachtgevers(ArrayList<String> opdrachtgevers) {
        this.opdrachtgevers = opdrachtgevers;
    }

    public ArrayList<String> getAfdelingen() {
        return afdelingen;
    }

    public void setAfdelingen(ArrayList<String> afdelingen) {
        this.afdelingen = afdelingen;
    }

    public ArrayList<String> getResultaten() {
        return resultaten;
    }

    public void setResultaten(ArrayList<String> resultaten) {
        this.resultaten = resultaten;
    }

    public ArrayList<String> getSoort_schades() {
        return soort_schades;
    }

    public void setSoort_schades(ArrayList<String> soort_schades) {
        this.soort_schades = soort_schades;
    }

    public String getDatum_start() {
        return datum_start;
    }

    public void setDatum_start(String datum_start) {
        this.datum_start = datum_start;
    }

    public String getDatum_eind() {
        return datum_eind;
    }

    public void setDatum_eind(String datum_eind) {
        this.datum_eind = datum_eind;
    }

    public int getKwalificatie() {
        return kwalificatie;
    }

    public void setKwalificatie(int kwalificatie) {
        this.kwalificatie = kwalificatie;
    }

    public int getBespaarBedrag() {
        return bespaarBedrag;
    }

    public void setBespaarBedrag(int bespaarBedrag) {
        this.bespaarBedrag = bespaarBedrag;
    }

    public int getFactuurBedrag() {
        return factuurBedrag;
    }

    public void setFactuurBedrag(int factuurBedrag) {
        this.factuurBedrag = factuurBedrag;
    }

    public int getExpertId() {
        return expertId;
    }

    public void setExpertId(int expertId) {
        this.expertId = expertId;
    }
}
