package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.security.Principal;
import java.util.ArrayList;
import nl.ced.View;
import nl.ced.persistence.PersoonDAO;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Meer informatie over validatie:
 *  http://hibernate.org/validator/
 * 
 * @author Peter van Vliet
 */
public class Persoon implements Principal
{
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private String voornaam;
    
    @JsonView(View.Public.class)
    private String achternaam;
    
    @JsonView(View.Public.class)
    private int active;
    
    @JsonIgnore
    private PersoonDAO dao;
    
    @JsonView(View.Public.class)
    private String tussenvoegsel;
    
    @JsonView(View.Public.class)
    private String telnr;
    
    @JsonView(View.Public.class)
    private int rol;
    
    @NotEmpty
    @JsonView(View.Public.class)
    private String email;
    
    @NotEmpty
    @JsonView(View.Protected.class)
    private String wachtwoord;
    
    public Persoon() {}

    @JsonIgnore
    public Persoon(int id, String voornaam, String achternaam, String tussenvoegsel,
            String email, String wachtwoord, String telnr, int rol, int active) {
        this.id = id;
        this.voornaam = voornaam;
        this.tussenvoegsel = tussenvoegsel;
        this.achternaam = achternaam;
        this.wachtwoord = wachtwoord;
        this.email = email;
        this.telnr = telnr;
        this.rol = rol;
        this.active = active;

    }

    public String getEmail(){
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWachtwoord(){
        return wachtwoord;
    }

    public void setWachtwoord(String password){
        this.wachtwoord = password;
    }

    @Override
    @JsonIgnore
    public String getName(){
        return voornaam;
    }
    
    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }
    
    public String getVoornaam() {
        return voornaam;
    }


    public PersoonDAO getDao() {
        return dao;
    }

    public String getTelnr() {
        return telnr;
    }
    public void setTelnr(String telnr) {
        this.telnr = telnr;
    }

    public String getTussenvoegsel() {
        return tussenvoegsel;
    }
    

   
    public boolean hasRole(int role) {
        return (role == this.rol);
    }
    

   
    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

 
    public String getAchternaam() {
        return achternaam;
    }

  
    public int getRol() {
        return rol;
    }

   
    public void setRol(int rol) {
        this.rol = rol;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
