package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import nl.ced.View;
import nl.ced.persistence.DossierDAO;

import java.security.Principal;

/**
 * 
 * @author Dylan Baars
 */
public class Dossier implements Principal
{
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private long nummer;
    
    @JsonView(View.Public.class)
    private int status_id;
    
    @JsonIgnore
    private DossierDAO dao;
    
    @JsonView(View.Public.class)
    private String opdrachtdatum;
    
    @JsonView(View.Public.class)
    private long polisnummer;
    
    @JsonView(View.Public.class)
    private int expert_id;
    
    @JsonView(View.Public.class)
    private int ogever_id;
    
    @JsonView(View.Public.class)
    private String nieuw_registratiedatum;
    
    @JsonView(View.Public.class)
    private String stratiedatum;
    
    @JsonView(View.Public.class)
    private int afdeling_id;
    
    @JsonView(View.Public.class)
    private int kwalificatie;
    
    @JsonView(View.Public.class)
    private int forensic_id;
    
    @JsonView(View.Public.class)
    private int soortschade_id;
    
    @JsonView(View.Public.class)
    private long schadenummer;
    
    @JsonView(View.Public.class)
    private Status status;
    
    @JsonView(View.Public.class)
    private Persoon expert;
   
    @JsonView(View.Public.class)
    private Opdrachtgever opdrachtgever;
    
    @JsonView(View.Public.class)
    private Forensic forensic;
    
    @JsonView(View.Public.class)
    private SoortSchade schade;
    
    @JsonView(View.Public.class)
    private Afdeling afdeling;
    @JsonView(View.Public.class)
    private String expert_naam;
    @JsonView(View.Public.class)
    private String expert_telefoon;
    @JsonView(View.Public.class)
    private String expert_mail;
    public Dossier() {}

    @JsonIgnore
    public Dossier(int id, long nummer, int status_id, String opdrachtdatum,
            long polisnummer, int ogever_id, String nieuw_registratiedatum,
            String stratiedatum, Afdeling afdeling, int kwalificatie,
            int forensic_id, int soortschade_id, long schadenummer, 
            Status status, Opdrachtgever opdrachtgever, 
            Forensic forensic, String expert_naam, 
            String expert_telefoon, String expert_mail, SoortSchade schade) {
        this.id = id;
        this.nummer = nummer;
        this.status_id = status_id;
        this.opdrachtdatum = opdrachtdatum;
        this.polisnummer = polisnummer;
        this.ogever_id = ogever_id;
        this.nieuw_registratiedatum = nieuw_registratiedatum;
        this.stratiedatum = stratiedatum;
        this.afdeling = afdeling;
        this.kwalificatie = kwalificatie;
        this.forensic_id = forensic_id;
        this.soortschade_id = soortschade_id;
        this.schadenummer = schadenummer;
        this.status = status;
        this.opdrachtgever = opdrachtgever;
        this.forensic = forensic;
        this.expert_naam = expert_naam;
        this.expert_telefoon = expert_telefoon;
        this.expert_mail = expert_mail;
        this.schade = schade;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public SoortSchade getSchade() {
        return schade;
    }

    public void setSchade(SoortSchade schade) {
        this.schade = schade;
    }

    public Afdeling getAfdeling() {
        return afdeling;
    }

    public void setAfdeling(Afdeling afdeling) {
        this.afdeling = afdeling;
    }

    @Override
    @JsonIgnore
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Forensic getForensic(){
        return forensic;
    }
    
    public void setForensic(Forensic forensic){
        this.forensic = forensic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getNummer() {
        return nummer;
    }

    public void setNummer(long nummer) {
        this.nummer = nummer;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public DossierDAO getDao() {
        return dao;
    }

    public void setDao(DossierDAO dao) {
        this.dao = dao;
    }

    public String getOpdrachtdatum() {
        return opdrachtdatum;
    }

    public void setOpdrachtdatum(String opdrachtdatum) {
        this.opdrachtdatum = opdrachtdatum;
    }

    public long getPolisnummer() {
        return polisnummer;
    }

    public void setPolisnummer(long polisnummer) {
        this.polisnummer = polisnummer;
    }

    public int getExpert_id() {
        return expert_id;
    }

    public void setExpert_id(int expert_id) {
        this.expert_id = expert_id;
    }

    public int getOgever_id() {
        return ogever_id;
    }

    public void setOgever_id(int ogever_id) {
        this.ogever_id = ogever_id;
    }

    public String getNieuw_registratiedatum() {
        return nieuw_registratiedatum;
    }

    public void setNieuw_registratiedatum(String nieuw_registratiedatum) {
        this.nieuw_registratiedatum = nieuw_registratiedatum;
    }

    public String getStratiedatum() {
        return stratiedatum;
    }

    public void setStratiedatum(String stratiedatum) {
        this.stratiedatum = stratiedatum;
    }
    
    public int getForensicObjectId(){
        return forensic.getId();
    }
    
    public int getAfdeling_id() {
        return afdeling_id;
    }

    public void setAfdeling_id(int afdeling_id) {
        this.afdeling_id = afdeling_id;
    }

    public int getKwalificatie() {
        return kwalificatie;
    }

    public void setKwalificatie(int kwalificatie) {
        this.kwalificatie = kwalificatie;
    }

    public int getForensic_id() {
        return forensic_id;
    }

    public void setForensic_id(int forensic_id) {
        this.forensic_id = forensic_id;
    }

    public int getSoortschade_id() {
        return soortschade_id;
    }

    public void setSoortschade_id(int soortschade_id) {
        this.soortschade_id = soortschade_id;
    }

    public long getSchadenummer() {
        return schadenummer;
    }

    public void setSchadenummer(long schadenummer) {
        this.schadenummer = schadenummer;
    }

    public int getStatusObjectId() {
        return this.status.getId();
    }

    public int getOpdrachtgeverObjectId() {
        try {
            return this.opdrachtgever.getId();
        } catch(Exception e){
            return 0;
        }
    }
    
    public int getAfdelingObjectId() {
        return this.afdeling.getId();
    }
    
    public int getAfdelingObjectType() {
        return this.afdeling.getType();
    }

    public Persoon getExpert() {
        return expert;
    }

    public void setExpert(Persoon expert) {
        this.expert = expert;
    }

    public Opdrachtgever getOpdrachtgever() {
        return opdrachtgever;
    }

    public void setOpdrachtgever(Opdrachtgever opdrachtgever) {
        this.opdrachtgever = opdrachtgever;
    }

    public String getExpert_naam() {
        return expert_naam;
    }

    public void setExpert_naam(String expert_naam) {
        this.expert_naam = expert_naam;
    }

    public String getExpert_telefoon() {
        return expert_telefoon;
    }

    public void setExpert_telefoon(String expert_telefoon) {
        this.expert_telefoon = expert_telefoon;
    }

    public String getExpert_mail() {
        return expert_mail;
    }

    public void setExpert_mail(String expert_mail) {
        this.expert_mail = expert_mail;
    }

}
