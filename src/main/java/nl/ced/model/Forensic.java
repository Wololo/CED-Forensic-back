package nl.ced.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.security.Principal;
import nl.ced.View;
import nl.ced.persistence.PersoonDAO;

/**
 * @author Dylan Baars
 */
public class Forensic implements Principal
{
    @JsonView(View.Public.class)
    private int id;
    
    @JsonView(View.Public.class)
    private long dossiernummer;
    
    @JsonView(View.Public.class)
    private int onderzoeker_id;
    
    @JsonView(View.Public.class)
    private int factuur_bedrag;
    
    @JsonView(View.Public.class)
    private int besparing;
    
    
    public Forensic() {
    }
    @JsonIgnore
    public Forensic(int id, long dossiernummer, int onderzoeker_id, 
            int factuur_bedrag, int besparing) {
        this.id = id;
        this.dossiernummer = dossiernummer;
        this.onderzoeker_id = onderzoeker_id;
        this.factuur_bedrag = factuur_bedrag;
        this.besparing = besparing;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getDossiernummer() {
        return dossiernummer;
    }

    public void setDossiernummer(long dossiernummer) {
        this.dossiernummer = dossiernummer;
    }

    public int getOnderzoeker_id() {
        return onderzoeker_id;
    }

    public void setOnderzoeker_id(int onderzoeker_id) {
        this.onderzoeker_id = onderzoeker_id;
    }

    public int getFactuur_bedrag() {
        return factuur_bedrag;
    }

    public void setFactuur_bedrag(int factuur_bedrag) {
        this.factuur_bedrag = factuur_bedrag;
    }


    public int getBesparing() {
        return besparing;
    }

    public void setBesparing(int besparing) {
        this.besparing = besparing;
    }

    @Override
    @JsonIgnore
    public String getName(){
        return null;
    }

}
