package nl.ced.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Singleton;
import nl.ced.View;
import nl.ced.model.*;
import nl.ced.service.FileService;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.InputStream;
import java.util.Collection;

/**
 * Meer informatie over resources:
 * https://jersey.java.net/documentation/latest/user-guide.html#jaxrs-resources
 *
 * @author Peter van Vliet
 */
@Singleton
@Path("/files")
@Produces(MediaType.APPLICATION_JSON)
public class FileResource {

    private final FileService service;

    @Inject
    public FileResource(FileService service) {

        this.service = service;   
    }
    
    @GET
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2", "3"})
    public Collection<Dossier> retrieveAll() {
        return service.getAll();
    }
    
    @GET
    @Path("/states")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2", "3"})
    public Collection<Status> retrieveStates() {
        return service.getStatus();
    }

    @GET
    @Path("/soortSchade/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2", "3"})
    public Collection<SoortSchade> retrieveSoortSchade(@PathParam("id") int id) {
        return service.getSoortSchade(id);
    }
    @GET
    @Path("/soortSchadeSingle/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1"})
    public SoortSchade retrieveSoortSchadeSingle(@PathParam("id") int id) {
        return service.getSoortSchadeSingle(id);
    }
    @GET
    @Path("/clients")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2", "3"})
    public Collection<Opdrachtgever> retrieveClients() {
        return service.getClients();
    }
    @GET
    @Path("/clients/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1"})
    public Opdrachtgever retrieveClient(@PathParam("id") int id) {
        return service.getClient(id);
    }
    @DELETE
    @Path("/clients/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1"})
    public int removeClient(@PathParam("id") int id) {
        return service.removeClient(id);
    }
    @GET
    @Path("/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1", "2", "3"})
    public Dossier retrieve(@PathParam("id") int id){
        return service.get(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/clientadd")
    @JsonView(View.Public.class)
    @RolesAllowed({"1"})
    public void createClient(Opdrachtgever opdrachtgever){
        service.createClient(opdrachtgever);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/client/{id}")
    @JsonView(View.Public.class)
    @RolesAllowed({"1"})
    public void updateClient(Opdrachtgever opdrachtgever){
        service.updateClient(opdrachtgever);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @Path("/property")
    @JsonView(View.Protected.class)
    //public void create(Dossier dossier) {
    public int createProperty(Afdeling afdeling) {
        return service.addProperty(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @Path("/updateProperty")
    @JsonView(View.Protected.class)
    public int updateProperty(Afdeling afdeling) {
        return service.updateProperty(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @Path("/mobility")
    @JsonView(View.Protected.class)
    //public void create(Dossier dossier) {
    public int createMobility(Afdeling afdeling) {
        return service.addMobility(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @Path("/updateMobility")
    @JsonView(View.Protected.class)
    public int updateMobility(Afdeling afdeling) {
        return service.updateMobility(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @Path("/vitality")
    @JsonView(View.Protected.class)
    public int createVitality(Afdeling afdeling) {
        return service.addVitality(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @Path("/updateVitality")
    @JsonView(View.Protected.class)
    //public void create(Dossier dossier) {
    public int updateVitality(Afdeling afdeling) {
        return service.updateVitality(afdeling);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @JsonView(View.Protected.class)
    //public void create(Dossier dossier) {
    public int create(Dossier dossier) {
        return(service.add(dossier));
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @Path("/update")
    @JsonView(View.Protected.class)
    public int update(Dossier dossier) {
        return(service.update(dossier));
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1"})
    @Path("/damageedit/{id}")
    @JsonView(View.Protected.class)
    public void damageedit(SoortSchade soortSchade) {
        service.updateSoortSchade(soortSchade);
    }
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1"})
    @Path("/damageremove/{id}")
    @JsonView(View.Protected.class)
    public int damagedelete(@PathParam("id") int id) {
        return service.deleteSoortSchade(id);
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1"})
    @Path("/damageadd")
    @JsonView(View.Protected.class)
    public int addSchade(SoortSchade schade) {
        return(service.addSchade(schade));
    }
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @RolesAllowed({"1", "2", "3"})
    @Path("/dossieropbouw/{id}")
    @JsonView(View.Protected.class)
    public Collection<DossierOpbouw> getOpbouw(@PathParam("id") int id) {
        return(service.getOpbouw(id));
    }
    @POST
    @Path("/uploadFile")
    @RolesAllowed({"1", "2", "3"})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public int uploadFile(@FormDataParam("file") InputStream fileInputStream,
         @FormDataParam("file") FormDataContentDisposition contentDispositionHeader,
         @FormDataParam("data") FormDataBodyPart opbouw) {
        opbouw.setMediaType(MediaType.APPLICATION_JSON_TYPE);
        DossierOpbouw opbouwObj = opbouw.getValueAs(DossierOpbouw.class);
        return service.saveOpbouw(fileInputStream, contentDispositionHeader, opbouwObj);
 
    }
    @GET
    @Path("/getFile/{id}")
    //@RolesAllowed({"1", "2"})
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getFile(@PathParam("id") int id) {
      File file = service.getFile(id); // Initialize this to the File path you want to serve.
      String filename = service.getFileName(id);
      return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM)
          .header("Content-Disposition", "attachment; filename=\""+filename+"\"" ) //optional
          .build();
    }
    @POST
    @Path("/getManagement")
    @RolesAllowed({"1"})
    @Consumes(MediaType.APPLICATION_JSON)
    public Collection<Dossier> getManagement(ManagementInfo info) {
        return service.getManagement(info);
    }
    @DELETE
    @RolesAllowed({"1", "2", "3"})
    @Path("/removeOpbouw/{id}")
    public int removeOpbouw(@PathParam("id") int id){
        return service.removeOpbouw(id);
    }
}
