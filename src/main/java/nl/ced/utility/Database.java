package nl.ced.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Reinier
 *
 * The database class makes connection with our database. It uses a
 * singleton-style pattern that makes sure only 1 connection is being made to
 * improve performance.
 */
public class Database {

    private String user = "postgres";
    private String password = "postgres";

    String url = "jdbc:postgresql://localhost:5432/database_mbz" +
            "?autoReconnect=true&allowMultiQueries=true&useSSL=false";

    /**
     * Returns een connection.
     * @return
     */
    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
//    public static Connection conn;
//    public static Statement stmt = null;
//    volatile static Database s;
//
//    /**
//     * The private constructor sets the conn variable with our db login
//     * information.
//     * @throws SQLException Throws an SQLException when it can't connect.
//     */
//    private Database() throws SQLException {
//        try {
//            Class.forName("org.postgresql.Driver"); 
//            String db_url = "jdbc:postgresql://de961.acugis-europa.com/wolologi_mbz" +
//                "?autoReconnect=true&allowMultiQueries=true&useSSL=false";
//            String db_user = "wolologi";
//            String db_password = "56aIsOzq39";
//            conn = DriverManager.getConnection(db_url, db_user, db_password);
//
//        } catch (ClassNotFoundException ex) {
//            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    /**
//     * Gets instance and initiates the first connection with the database.
//     *
//     * @return the instance
//     * @throws SQLException the sql exception
//     */
//    public static Database getInstance() throws SQLException {
//        if (s == null) {
//            synchronized (Database.class) {
//                if (s == null) {
//                    s = new Database();
//                }
//            }
//        }
//        conn = s.getConnection();
//        return s;
//    }
//
//    /**
//     * Gets connection.
//     *
//     * @return the connection
//     */
//    public Connection getConnection() {
//        return conn;
//    }
