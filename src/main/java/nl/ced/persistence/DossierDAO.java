/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ced.persistence;

import nl.ced.model.*;
import nl.ced.utility.Database;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 *
 * @author Dylan
 */
public class DossierDAO {
    private Database manager;
    private Connection connection;
    private PersoonDAO persoonDao;
    private PreparedStatement select;
    private PreparedStatement insert;
    private PreparedStatement update;
    private PreparedStatement delete;
    private PreparedStatement check;
    private List<Dossier> dossiers = new ArrayList<>();
    private List<Status> status = new ArrayList<>();
    private List<Opdrachtgever> opdrachtgevers = new ArrayList<>();
    private List<Afdeling> afdelingen = new ArrayList<>();

    private String datumVanTotString = "";
    private String opdachtgeverString = "";
    private String afdelingString = "";
    private String resultaatString = "";
    private String soortSchadeString = "";
    private String kwalificatieString = "";
    private String expertString = "";
    private String joinExpertString = "";


    public DossierDAO() {
        this.manager = new Database();
        updateStatusList();
    }

    public void updateStatusList() {
        Connection connection = manager.getConnection();
        status = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT status_id, status_beschrijving"
                            + " FROM status;");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    status.add(new Status(
                            resultSet.getString("status_beschrijving"),
                            resultSet.getInt("status_id")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
    }
    public String getAfdelingTypeNaam(int afdeling_id, Connection connection) {
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT afdeling_type FROM afdeling_type WHERE id = ?");
            select.setInt(1, afdeling_id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    return resultSet.getString("afdeling_type");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int createDossier(Dossier dossier) {
        Connection connection = manager.getConnection();
        int id = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO dossier ("
                    + "dossier_nummer,dossier_status_id,dossier_opdrachtdatum,"
                    + "dossier_polisnummer, dossier_ogever_id,"
                    + "dossier_nieuw_registratiedatum, dossier_stratiedatum,"
                    + "dossier_kwalificatie,dossier_schadenummer,"
                    + "afdeling_type,afdeling_id,"
                    + "expert_naam,expert_telefoon,expert_mail,dossier_soortschade_id)"
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
                    + "RETURNING dossier_id;");
            insert.setLong(1, dossier.getNummer());
            insert.setInt(2, dossier.getStatusObjectId());
            insert.setString(3, dossier.getOpdrachtdatum());
            insert.setLong(4, dossier.getPolisnummer());
            insert.setInt(5, dossier.getOpdrachtgeverObjectId());
            insert.setString(6, dossier.getNieuw_registratiedatum());
            insert.setString(7, dossier.getStratiedatum());
            insert.setInt(8, dossier.getKwalificatie());
            insert.setLong(9, dossier.getSchadenummer());
            insert.setInt(10, dossier.getAfdelingObjectType());
            insert.setInt(11, dossier.getAfdelingObjectId());
            insert.setString(12, dossier.getExpert_naam());
            insert.setString(13, dossier.getExpert_telefoon());
            insert.setString(14, dossier.getExpert_mail());
            insert.setInt(15, dossier.getSoortschade_id());
            ResultSet rs = insert.executeQuery();
            rs.next();
            id = rs.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }

            return id;
        }
    }

    public int updateDossier(Dossier dossier) {
        Connection connection = manager.getConnection();
        int forensicID = 0;
        if(dossier.getForensicObjectId() == 0){
            forensicID = createForensic(dossier.getForensic());
        } else {
            forensicID = updateForensic(dossier.getForensic());
        }
        try {
            update = connection.prepareStatement(
                    "UPDATE dossier SET dossier_nummer = ?,"
                            + "dossier_status_id = ?, "
                            + "dossier_opdrachtdatum = ?,  "
                            + "dossier_polisnummer = ?, "
                            + "dossier_ogever_id = ?, "
                            + "dossier_nieuw_registratiedatum = ?, "
                            + "dossier_stratiedatum = ?, "
                            + "dossier_kwalificatie = ?, "
                            + "dossier_schadenummer = ?, "
                            + "expert_naam = ?, "
                            + "expert_telefoon = ?, "
                            + "expert_mail = ?, "
                            + "dossier_forensic_id = ?, "
                            + "dossier_soortschade_id = ? "
                            + "WHERE dossier_id = ?;");
            update.setLong(1, dossier.getNummer());
            update.setInt(2, dossier.getStatusObjectId());
            update.setString(3, dossier.getOpdrachtdatum());
            update.setLong(4, dossier.getPolisnummer());
            update.setInt(5, dossier.getOpdrachtgeverObjectId());
            update.setString(6, dossier.getNieuw_registratiedatum());
            update.setString(7, dossier.getStratiedatum());
            update.setInt(8, dossier.getKwalificatie());
            update.setLong(9, dossier.getSchadenummer());
            update.setString(10, dossier.getExpert_naam());
            update.setString(11, dossier.getExpert_telefoon());
            update.setString(12, dossier.getExpert_mail());
            update.setInt(13, forensicID);
            update.setInt(14, dossier.getSoortschade_id());
            update.setInt(15, dossier.getId());
            update.executeUpdate();
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            try {
                update.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 1;
    }


    private void managementinfoDatums(String van, String tot){
        //'2017-01-01' van
        // '2017-12-30' tot
        datumVanTotString = "( to_date(dossier_opdrachtdatum, 'dd-mm-yyyy') BETWEEN '" + van + "' AND '" + tot + "') " +
                "AND ";
//        datumVanTotString = "(d.dossier_opdrachtdatum between " + van + " AND " + tot + ") AND ";
    }

    private void managementInfoOpdrachtgever(ArrayList<String> s){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");
        for (int i = 0; i < s.size(); i++) {
            stringBuilder.append("dossier_ogever_id = " + s.get(i));
            if(i != s.size() - 1){
                stringBuilder.append(" OR ");
            }
        }
        stringBuilder.append(") AND ");
        opdachtgeverString =  stringBuilder.toString();

    }


    private void managementInfoAfdeling(ArrayList<String> s){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");
        for (int i = 0; i < s.size(); i++) {
            stringBuilder.append("afdeling_type = " + s.get(i));
            if(i != s.size() - 1){
                stringBuilder.append(" OR ");
            }
        }
        stringBuilder.append(") AND ");
        afdelingString =  stringBuilder.toString();
    }
    private void managementInfoKwalificatie(int... s){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");
        for (int i = 0; i < s.length; i++) {
            if(s[i] == 0){}else{
                stringBuilder.append("dossier_kwalificatie = " + s[i]);
                if(i != s.length - 1){
                    stringBuilder.append(" OR ");
                }
            }
        }
        stringBuilder.append(") AND ");
        kwalificatieString =  stringBuilder.toString();
    }
//" JOIN forensic ON dossier_forensic_id = forensic_id WHERE forensic_onderzoeker_id="+s[i]
    private void managementInfoExpert(int... s){
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append("(");
        for (int i = 0; i < s.length; i++) {
            if(s[i] == 0){
                
            } else {
                stringBuilder.append("forensic_onderzoeker_id="+s[i]);
                joinExpertString = "JOIN forensic ON dossier_forensic_id = forensic_id ";
                if(i != s.length - 1){
                    stringBuilder.append(" or ");
                }
            }
        }
        stringBuilder.append(") AND ");
        expertString =  stringBuilder.toString();
    }

    private String mIString(){
        String s = null;
        if(opdachtgeverString.endsWith("() AND ")){
            opdachtgeverString = "";
        }
        if(afdelingString.endsWith("() AND ")){
            afdelingString = "";
        }

        if(kwalificatieString.endsWith("() AND ")){
            kwalificatieString = "";
        }
        if(expertString.endsWith("() AND ")){
            expertString = "";
        }
        s = datumVanTotString + opdachtgeverString + afdelingString + kwalificatieString + expertString;

        if(s.endsWith(" AND ")){
            s = s.substring(0, s.length() - 5);
        }
        return s;
    }

    public Collection<Dossier> getManagementInfo(ManagementInfo info){
        joinExpertString = "";
        managementinfoDatums(info.getDatum_start(), info.getDatum_eind());
        managementInfoOpdrachtgever(info.getOpdrachtgevers());
        managementInfoAfdeling(info.getAfdelingen());
        managementInfoKwalificatie(info.getKwalificatie());
        managementInfoExpert(info.getExpertId());

        dossiers = new ArrayList();
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        Dossier dossier = null;

        Afdeling afdeling = null;
        Forensic tempFor = null;

        try {
            select = connection.prepareStatement(
                    "SELECT * "
                            + "FROM dossier "
                            + joinExpertString
                            + "WHERE " + mIString() + "");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
                if (resultSet != null) {

                    while (resultSet.next()) {
                    SoortSchade tempSch = null;
                    if (resultSet.getInt("dossier_soortschade_id") > 0) {
                        tempSch = getSoortSchade(resultSet.getInt("dossier_soortschade_id"), connection);
                    } else {
                        tempSch = new SoortSchade();
                    }
                    if (resultSet.getInt("dossier_forensic_id") > 0) {
                        tempFor = getForensic(resultSet.getInt("dossier_forensic_id"), connection);
                    } else {
                        tempFor = new Forensic();
                    }
                    if (resultSet.getInt("afdeling_type") > 0){
                        switch (getAfdelingTypeNaam(resultSet.getInt("afdeling_type"), connection)) {
                            case "vitality":
                                afdeling = getVitality(resultSet.getInt("afdeling_id"), connection);
                                break;
                            case "mobility":
                                afdeling = getMobility(resultSet.getInt("afdeling_id"), connection);
                                break;
                            case "property":
                                afdeling = getProperty(resultSet.getInt("afdeling_id"), connection);
                                break;
                            default:
                                break;
                        }
                    } else {
                        afdeling = new Afdeling();
                    }
                        dossiers.add(new Dossier(
                                resultSet.getInt("dossier_id"),
                                resultSet.getLong("dossier_nummer"),
                                resultSet.getInt("dossier_status_id"),
                                resultSet.getString("dossier_opdrachtdatum"),
                                resultSet.getLong("dossier_polisnummer"),
                                resultSet.getInt("dossier_ogever_id"),
                                resultSet.getString("dossier_nieuw_registratiedatum"),
                                resultSet.getString("dossier_stratiedatum"),
                                afdeling,
                                resultSet.getInt("dossier_kwalificatie"),
                                resultSet.getInt("dossier_forensic_id"),
                                resultSet.getInt("dossier_soortschade_id"),
                                resultSet.getLong("dossier_schadenummer"),
                                getStatus(resultSet.getInt("dossier_status_id"), connection),
                                getOpdrachtgever(resultSet.getInt("dossier_ogever_id"), connection),
                                tempFor,
                                resultSet.getString("expert_naam"),
                                resultSet.getString("expert_telefoon"),
                                resultSet.getString("expert_mail"),
                                getSoortSchade(resultSet.getInt("dossier_soortschade_id"), connection)
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return dossiers;
    }


    public boolean createClient(Opdrachtgever opdrachtgever) {
        Connection connection = manager.getConnection();
        try {
            insert = connection.prepareStatement("INSERT INTO opdrachtgever "
                    + "(ogever_naam, ogever_telefoonnr, ogever_contactpersoon)"
                    + "VALUES (?,?,?)");
            insert.setString(1, opdrachtgever.getNaam());
            insert.setString(2, opdrachtgever.getTelefoonnummer());
            insert.setString(3, opdrachtgever.getContactpersoonnaam());
            insert.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }



            return true;
        }
    }

    public int addProperty(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        int a = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO property "
                    + "(property_adres, property_plaats, "
                    + "property_huisnr, property_toevoeging, property_type, postcode,"
                    + "naam_verzekerde)"
                    + "VALUES (?,?,?,?,?,?,?) "
                    + "RETURNING property_id;");
            insert.setString(1, afdeling.getProperty_adres());
            insert.setString(2, afdeling.getProperty_plaats());
            insert.setString(3, afdeling.getProperty_huisnr());
            insert.setString(4, afdeling.getProperty_toevoeging());
            insert.setString(5, afdeling.getProperty_type());
            insert.setString(6, afdeling.getPostcode());
            insert.setString(7, afdeling.getNaam_verzekerde());

            ResultSet rs = insert.executeQuery();
            rs.next();
            a = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }


            return a;
        }
    }

    public int addMobility(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        int a = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO mobility "
                    + "(mobility_kenteken, mobility_automaat, "
                    + "postcode, huisnr, naam_verzekerde)"
                    + "VALUES (?,?,?,?,?) "
                    + "RETURNING mobility_id;");
            insert.setString(1, afdeling.getMobility_kenteken());
            insert.setInt(2, afdeling.getMobility_automaat());
            insert.setString(3, afdeling.getPostcode());
            insert.setString(4, afdeling.getHuisnr());
            insert.setString(5, afdeling.getNaam_verzekerde());
            ResultSet rs = insert.executeQuery();
            rs.next();
            a = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }

            return a;
        }
    }

    public int addVitality(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        int a = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO vitality "
                    + "(vitality_slachtoffernaam, vitality_beschrijving, "
                    + "postcode, huisnr)"
                    + "VALUES (?,?,?,?) "
                    + "RETURNING vitality_id;");
            insert.setString(1, afdeling.getVitality_slachtoffernaam());
            insert.setString(2, afdeling.getVitality_beschrijving());
            insert.setString(3, afdeling.getPostcode());
            insert.setString(4, afdeling.getHuisnr());
            ResultSet rs = insert.executeQuery();
            rs.next();
            a = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
            return a;
        }
    }

    public Forensic getForensic(int id, Connection connection) {
        ResultSet resultSet = null;
        Forensic forensic = null;
        try {
            select = connection.prepareStatement(
                    "SELECT *"
                            + " FROM forensic WHERE forensic_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {

                    forensic = new Forensic(resultSet.getInt("forensic_id"),
                            resultSet.getLong("forensic_dossiernr"),
                            resultSet.getInt("forensic_onderzoeker_id"),
                            resultSet.getInt("forensic_factuurbedrag"),
                            resultSet.getInt("forensic_besparing"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return forensic;
    }

    public Status getStatus(int id, Connection connection) {
        ResultSet resultSet = null;
        Status status = null;
        try {
            select = connection.prepareStatement(
                    "SELECT status_id, status_beschrijving FROM status WHERE status_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    status = new Status(resultSet.getString("status_beschrijving"), resultSet.getInt("status_id"));
                }
            }
        } catch (SQLException e) {
            //
        }
        return status;
    }

    public Opdrachtgever getOpdrachtgever(int id, Connection connection) {
        ResultSet resultSet = null;
        Opdrachtgever opdrachtgever = null;
        try {
            select = connection.prepareStatement(
                    "SELECT ogever_id, ogever_naam, ogever_telefoonnr, "
                            + "ogever_contactpersoon, ogever_contactpersoon_telnr FROM opdrachtgever"
                            + " WHERE ogever_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    opdrachtgever = new Opdrachtgever(resultSet.getInt("ogever_id"),
                            resultSet.getString("ogever_naam"),
                            resultSet.getString("ogever_telefoonnr"),
                            resultSet.getString("ogever_contactpersoon"),
                            resultSet.getString("ogever_contactpersoon_telnr"));
                }
            }
        } catch (SQLException e) {
            //
        }
        return opdrachtgever;
    }

    public Dossier get(int id) {
        Connection connection = manager.getConnection();
        Dossier dossier;
        Afdeling afdeling = null;
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT dossier_id,dossier_nummer,dossier_status_id,dossier_opdrachtdatum,"
                            + "dossier_polisnummer, dossier_ogever_id, "
                            + "dossier_nieuw_registratiedatum, dossier_stratiedatum,"
                            + "dossier_kwalificatie,"
                    + "dossier_forensic_id,dossier_soortschade_id,dossier_schadenummer, "
                    + "afdeling_type,afdeling_id, expert_naam, expert_telefoon,"
                            + "expert_mail"
                    + " FROM dossier WHERE dossier_id = ?;");
            select.setInt(1, id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (resultSet != null) {
                
                while (resultSet.next()) {
                    Forensic tempFor = null;
                    SoortSchade tempSch = null;
                    if (resultSet.getInt("dossier_soortschade_id") > 0) {
                        tempSch = getSoortSchade(resultSet.getInt("dossier_soortschade_id"), connection);
                    } else {
                        tempSch = new SoortSchade();
                    }
                    if (resultSet.getInt("dossier_forensic_id") > 0) {
                        tempFor = getForensic(resultSet.getInt("dossier_forensic_id"), connection);
                    } else {
                        tempFor = new Forensic();
                    }
                    if (resultSet.getInt("afdeling_type") > 0){
                        switch (getAfdelingTypeNaam(resultSet.getInt("afdeling_type"), connection)) {
                            case "vitality":
                                afdeling = getVitality(resultSet.getInt("afdeling_id"), connection);
                                break;
                            case "mobility":
                                afdeling = getMobility(resultSet.getInt("afdeling_id"), connection);
                                break;
                            case "property":
                                afdeling = getProperty(resultSet.getInt("afdeling_id"), connection);
                                break;
                            default:
                                break;
                        }
                    } else {
                        afdeling = new Afdeling();
                    }
                    dossier = new Dossier(
                            resultSet.getInt("dossier_id"),
                            resultSet.getLong("dossier_nummer"),
                            resultSet.getInt("dossier_status_id"),
                            resultSet.getString("dossier_opdrachtdatum"),
                            resultSet.getLong("dossier_polisnummer"),
                            resultSet.getInt("dossier_ogever_id"),
                            resultSet.getString("dossier_nieuw_registratiedatum"),
                            resultSet.getString("dossier_stratiedatum"),
                            afdeling,
                            resultSet.getInt("dossier_kwalificatie"),
                            resultSet.getInt("dossier_forensic_id"),
                            resultSet.getInt("dossier_soortschade_id"),
                            resultSet.getLong("dossier_schadenummer"),
                            getStatus(resultSet.getInt("dossier_status_id"), connection),
                            getOpdrachtgever(resultSet.getInt("dossier_ogever_id"), connection),
                            tempFor,
                            resultSet.getString("expert_naam"),
                            resultSet.getString("expert_telefoon"),
                            resultSet.getString("expert_mail"),
                            tempSch
                    );
                    return dossier;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {  
                
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        
//        for (Dossier d : dossiers) {
//            if (d.getId() == id) {
//                return d;
//            }
//        }
//        return null;
        return null;
    }

    public List<Dossier> getAll() {
        Connection connection = manager.getConnection();
        dossiers = new ArrayList<>();
        
        Afdeling afdeling = null;
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT dossier_id,dossier_nummer,dossier_status_id,dossier_opdrachtdatum,"
                            + "dossier_polisnummer, dossier_ogever_id, "
                            + "dossier_nieuw_registratiedatum, dossier_stratiedatum,"
                            + "dossier_kwalificatie,"
                            + "dossier_forensic_id,dossier_soortschade_id,dossier_schadenummer, "
                            + "afdeling_type,afdeling_id, expert_naam, expert_telefoon,"
                            + "expert_mail"
                            + " FROM dossier;");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (resultSet != null) {
                
                while (resultSet.next()) {
                    Forensic tempFor = null;
                    SoortSchade tempSch = null;
                    if (resultSet.getInt("dossier_soortschade_id") > 0) {
                        tempSch = getSoortSchade(resultSet.getInt("dossier_soortschade_id"), connection);
                    } else {
                        tempSch = new SoortSchade();
                    }
                    if (resultSet.getInt("dossier_forensic_id") > 0) {
                        tempFor = getForensic(resultSet.getInt("dossier_forensic_id"), connection);
                    } else {
                        tempFor = new Forensic();
                    }
                    if (resultSet.getInt("afdeling_type") > 0){
                        switch (getAfdelingTypeNaam(resultSet.getInt("afdeling_type"), connection)) {
                            case "vitality":
                                afdeling = getVitality(resultSet.getInt("afdeling_id"), connection);
                                break;
                            case "mobility":
                                afdeling = getMobility(resultSet.getInt("afdeling_id"), connection);
                                break;
                            case "property":
                                afdeling = getProperty(resultSet.getInt("afdeling_id"), connection);
                                break;
                            default:
                                break;
                        }
                    } else {
                        afdeling = new Afdeling();
                    }
                    dossiers.add(new Dossier(
                            resultSet.getInt("dossier_id"),
                            resultSet.getLong("dossier_nummer"),
                            resultSet.getInt("dossier_status_id"),
                            resultSet.getString("dossier_opdrachtdatum"),
                            resultSet.getLong("dossier_polisnummer"),
                            resultSet.getInt("dossier_ogever_id"),
                            resultSet.getString("dossier_nieuw_registratiedatum"),
                            resultSet.getString("dossier_stratiedatum"),
                            afdeling,
                            resultSet.getInt("dossier_kwalificatie"),
                            resultSet.getInt("dossier_forensic_id"),
                            resultSet.getInt("dossier_soortschade_id"),
                            resultSet.getLong("dossier_schadenummer"),
                            getStatus(resultSet.getInt("dossier_status_id"), connection),
                            getOpdrachtgever(resultSet.getInt("dossier_ogever_id"), connection),
                            tempFor,
                            resultSet.getString("expert_naam"),
                            resultSet.getString("expert_telefoon"),
                            resultSet.getString("expert_mail"),
                            tempSch
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {  
                
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return dossiers;
    }

    public List<Status> getStates() {
        return status;
    }

    public SoortSchade getSoortSchade(int id, Connection connection){
        ResultSet resultSet = null;
        SoortSchade soortSchade = null;
        try {
            select = connection.prepareStatement(
                    "SELECT schade_id, schade_naam, schade_afdeling_id"
                            + " FROM soort_schade WHERE schade_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {

                    soortSchade = new SoortSchade(
                            resultSet.getInt("schade_id"),
                            resultSet.getInt("schade_afdeling_id"),
                            resultSet.getString("schade_naam")    
                    );
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return soortSchade;
    }
    public SoortSchade getSoortSchadeFromWeb(int id){
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        SoortSchade soortSchade = null;
        try {
            select = connection.prepareStatement(
                    "SELECT schade_id, schade_naam, schade_afdeling_id"
                            + " FROM soort_schade WHERE schade_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {

                    soortSchade = new SoortSchade(
                            resultSet.getInt("schade_id"),
                            resultSet.getInt("schade_afdeling_id"),
                            resultSet.getString("schade_naam")    
                    );
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try{
                connection.close();
            } catch(SQLException e){}
        }
        return soortSchade;
    }
    public List<Opdrachtgever> getClients() {
        Connection connection = manager.getConnection();
        opdrachtgevers = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT ogever_id, ogever_naam, ogever_telefoonnr, "
                            + "ogever_contactpersoon, ogever_contactpersoon_telnr"
                            + " FROM opdrachtgever;");
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    opdrachtgevers.add(new Opdrachtgever(
                            resultSet.getInt("ogever_id"),
                            resultSet.getString("ogever_naam"),
                            resultSet.getString("ogever_telefoonnr"),
                            resultSet.getString("ogever_contactpersoon"),
                            resultSet.getString("ogever_contactpersoon_telnr")
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return opdrachtgevers;
    }

    public void archive(int id) {
        Connection connection = manager.getConnection();
        try {
            select = connection.prepareStatement(
                    "UPDATE dossier SET dossier_status_id = '4' WHERE dossier_id = ?;");
            select.setInt(1, id);
            select.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
    }

    public void recover(int id) {
        Connection connection = manager.getConnection();
        try {
            select = connection.prepareStatement(
                    "UPDATE dossier SET dossier_status_id = 1 WHERE dossier_id = ?;");
            select.setInt(1, id);
            select.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private Afdeling getProperty(int id, Connection connection) {
        ResultSet resultSet = null;
        Afdeling afdeling = null;
        try {
            select = connection.prepareStatement(
                    "SELECT property_id, property_adres, "
                            + "property_plaats, property_huisnr, "
                            + "property_toevoeging, property_type, postcode, "
                            + "naam_verzekerde "
                            + "FROM property WHERE property_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    afdeling = new Afdeling(
                            resultSet.getInt("property_id"),
                            resultSet.getString("property_adres"),
                            resultSet.getString("property_plaats"),
                            resultSet.getString("property_huisnr"),
                            resultSet.getString("property_toevoeging"),
                            resultSet.getString("property_type"),
                            resultSet.getString("postcode"),
                            resultSet.getString("naam_verzekerde")
                    );
                }
            }
        } catch (SQLException e) {
            //
        }
        return afdeling;
    }

    private Afdeling getMobility(int id, Connection connection) {
        ResultSet resultSet = null;
        Afdeling afdeling = null;
        try {
            select = connection.prepareStatement(
                    "SELECT mobility_id, mobility_kenteken, "
                            + "mobility_automaat, postcode, "
                            + "huisnr, naam_verzekerde "
                            + "FROM mobility WHERE mobility_id = ?");
            select.setInt(1, id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    afdeling = new Afdeling(
                            resultSet.getInt("mobility_id"),
                            resultSet.getString("mobility_kenteken"),
                            resultSet.getInt("mobility_automaat"),
                            resultSet.getString("postcode"),
                            resultSet.getString("naam_verzekerde"),
                            resultSet.getString("huisnr")
                    );
                }
            }
        } catch (SQLException e) {
            //
        }
        return afdeling;
    }

    public void setStatus(int id, int status) {
        Connection connection = manager.getConnection();
        try {
            select = connection.prepareStatement(
                    "UPDATE dossier SET dossier_status_id = ? WHERE dossier_id = ?;");
            select.setInt(1, status);
            select.setInt(2, id);
            select.executeUpdate();
        } catch (SQLException e) {

        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private Afdeling getVitality(int id, Connection connection) {
        ResultSet resultSet = null;
        Afdeling afdeling = null;
        try {
            select = connection.prepareStatement(
                    "SELECT vitality_id, vitality_slachtoffernaam, "
                            + "vitality_beschrijving, postcode, huisnr "
                            + "FROM vitality WHERE vitality_id = ?");
            select.setInt(1, id);

            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    afdeling = new Afdeling(
                            resultSet.getInt("vitality_id"),
                            resultSet.getString("vitality_slachtoffernaam"),
                            resultSet.getString("vitality_beschrijving"),
                            resultSet.getString("postcode"),
                            resultSet.getString("huisnr")
                    );

                }
            }
        } catch (SQLException e) {
            //
        }
        return afdeling;
    }

    public int updateVitality(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        try {
            update = connection.prepareStatement(
                    "UPDATE vitality SET "
                            + "vitality_slachtoffernaam = ?, "
                            + "vitality_beschrijving = ?, "
                            + "postcode = ?, "
                            + "huisnr = ? "
                            + "WHERE vitality_id = ?;");
            update.setString(1, afdeling.getVitality_slachtoffernaam());
            update.setString(2, afdeling.getVitality_beschrijving());
            update.setString(3, afdeling.getPostcode());
            update.setString(4, afdeling.getHuisnr());
            update.setInt(5, afdeling.getId());
            update.executeUpdate();
        } catch (SQLException e) {

        } finally {
            try {
                update.close();
                connection.close();
                return 1;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int updateMobility(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        try {
            update = connection.prepareStatement(
                    "UPDATE mobility SET "
                            + "mobility_kenteken = ?, "
                            + "naam_verzekerde = ?, "
                            + "postcode = ?, "
                            + "huisnr = ? "
                            + "WHERE mobility_id = ?;");
            update.setString(1, afdeling.getMobility_kenteken());
            update.setString(2, afdeling.getNaam_verzekerde());
            update.setString(3, afdeling.getPostcode());
            update.setString(4, afdeling.getHuisnr());
            update.setInt(5, afdeling.getId());
            update.executeUpdate();
        } catch (SQLException e) {

        } finally {
            try {
                update.close();
                connection.close();
                return 1;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public int updateProperty(Afdeling afdeling) {
        Connection connection = manager.getConnection();
        try {
            update = connection.prepareStatement(
                    "UPDATE property SET "
                            + "property_adres = ?, "
                            + "property_plaats = ?, "
                            + "property_huisnr = ?, "
                            + "property_toevoeging = ?, "
                            + "property_type = ?, "
                            + "postcode = ?, "
                            + "naam_verzekerde = ? "
                            + "WHERE property_id = ?;");
            update.setString(1, afdeling.getProperty_adres());
            update.setString(2, afdeling.getProperty_plaats());
            update.setString(3, afdeling.getProperty_huisnr());
            update.setString(4, afdeling.getProperty_toevoeging());
            update.setString(5, afdeling.getProperty_type());
            update.setString(6, afdeling.getPostcode());
            update.setString(7, afdeling.getNaam_verzekerde());
            update.setInt(8, afdeling.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                update.close();
                connection.close();
                return 1;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    private int createForensic(Forensic forensic) {
        Connection connection = manager.getConnection();
        int a = 0;
        try {
            insert = connection.prepareStatement("INSERT INTO forensic ("
                    + "forensic_dossiernr, "
                    + "forensic_onderzoeker_id, "
                    + "forensic_factuurbedrag, "
                    + "forensic_besparing"
                    + ")"
                    + "VALUES (?,?,?,?) "
                    + "RETURNING forensic_id;");
            insert.setLong(1, forensic.getDossiernummer());
            insert.setInt(2, forensic.getOnderzoeker_id());
            insert.setInt(3, forensic.getFactuur_bedrag());
            insert.setInt(4, forensic.getBesparing());
            ResultSet rs = insert.executeQuery();
            rs.next();
            a = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
            return a;
        }
    }

    private int updateForensic(Forensic forensic) {
        Connection connection = manager.getConnection();
        try {
            update = connection.prepareStatement(
                    "UPDATE forensic SET "
                            + "forensic_dossiernr = ?, "
                            + "forensic_onderzoeker_id = ?, "
                            + "forensic_factuurbedrag = ?, "
                            + "forensic_besparing = ? "
                            + "WHERE forensic_id = ?;");
            update.setLong(1, forensic.getDossiernummer());
            update.setInt(2, forensic.getOnderzoeker_id());
            update.setDouble(3, forensic.getFactuur_bedrag());
            update.setDouble(4, forensic.getBesparing());
            update.setInt(5, forensic.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                update.close();
                connection.close();
                return forensic.getId();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public List<DossierOpbouw> getOpbouw(int id){
        ArrayList<DossierOpbouw> opbouw = new ArrayList();
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT id, naam, "
                            + "extension, titel, "
                            + "beschrijving, datum_toevoeging  "
                            + "FROM dossier_opbouw WHERE dossier_id = ? ORDER BY id DESC");
            select.setInt(1, id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            //
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    opbouw.add(new DossierOpbouw(resultSet.getInt("id"),
                            resultSet.getString("naam"),
                            resultSet.getString("extension"),
                            resultSet.getString("beschrijving"),
                            resultSet.getString("titel"),
                            resultSet.getString("datum_toevoeging")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                
                connection.close();
            } catch (SQLException e) {
                //
            }
        }


        return opbouw;
    }
    public int saveOpbouw(InputStream fileInputStream, FormDataContentDisposition contentDispositionHeader, DossierOpbouw opbouw){

        Connection connection = manager.getConnection();
        String theBase;
        String theExtension;
        if(contentDispositionHeader.getFileName() != null){
            theBase = getBaseName(contentDispositionHeader.getFileName());
            theExtension = getFileExtension(contentDispositionHeader.getFileName());
        } else {
            theBase = null;
            theExtension = null;
        }
        try {

            insert = connection.prepareStatement("INSERT INTO dossier_opbouw "
                    + "(naam, extension, files, dossier_id, titel, "
                    + "beschrijving, datum_toevoeging)"
                    + "VALUES (?,?,?,?,?,?,?)");
            insert.setString(1, theBase);
            insert.setString(2, theExtension);
            insert.setBinaryStream(3, fileInputStream);
            insert.setInt(4, opbouw.getId());
            insert.setString(5, opbouw.getTitel());
            insert.setString(6, opbouw.getBeschrijving());
            insert.setString(7, opbouw.getDatum_toevoeging());
            insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                insert.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 0;
            }
            return 1;
        }

    }

    public File getFileFromDB(int id) throws IOException{
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT id, naam, extension, files "
                            + "FROM dossier_opbouw WHERE id = ? ");
            select.setInt(1, id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet.next()) {
                return getFile(
                        resultSet.getBinaryStream("files"),
                        resultSet.getString("naam"),
                        resultSet.getString("extension")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                
                connection.close();
            } catch (SQLException e) {
                //
            }
        }


        return null;
    }

    public File getFile(InputStream in, String name, String extension) throws IOException{
        final File tempFile = File.createTempFile(name,extension);

        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }
    private static String getFileExtension(String file) {

        if(file.lastIndexOf(".") != -1 && file.lastIndexOf(".") != 0)
            return file.substring(file.lastIndexOf("."));
        else return "";
    }
    private static String getBaseName(String file) {

        int pos = file.lastIndexOf(".");
        if (pos > 0) {
            file = file.substring(0, pos);
        }
        return file;
    }


    public String getFileName(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        String filename;
        try {
            select = connection.prepareStatement(
                    "SELECT naam, extension "
                            + "FROM dossier_opbouw WHERE id = ? ");
            select.setInt(1, id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet.next()) {
                filename = resultSet.getString("naam")+resultSet.getString("extension");
                return filename;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return null;
    }

    public int removeOpbouw(int id) {
        Connection connection = manager.getConnection();
        try {
            select = connection.prepareStatement(
                    "DELETE  "
                            + "FROM dossier_opbouw WHERE id = ? ");
            select.setInt(1, id);
            select.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                
                connection.close();
                return 1;
            } catch (SQLException e) {
                //
            }
        }
        return 0;
    }

    public Collection<SoortSchade> getSoortSchadeList(int id) {
        Connection connection = manager.getConnection();
        ResultSet resultSet = null;
        List<SoortSchade> soortSchade = new ArrayList<>();
        try {
            select = connection.prepareStatement(
                    "SELECT schade_id, schade_naam, schade_afdeling_id"
                            + " FROM soort_schade WHERE schade_afdeling_id = ?");
            select.setInt(1, id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    soortSchade.add(new SoortSchade(
                            resultSet.getInt("schade_id"),
                            resultSet.getInt("schade_afdeling_id"),
                            resultSet.getString("schade_naam")    
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                //
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return soortSchade;  
    }

    public int addSchade(SoortSchade schade) {
        Connection connection = manager.getConnection();
        try {
            insert = connection.prepareStatement("INSERT INTO soort_schade (schade_afdeling_id, schade_naam) " +
                    "VALUES(?,?);");
            insert.setInt(1,schade.getAfdeling());
            insert.setString(2,schade.getNaam());
            insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return 1;
            }
            return 0;
        }
    }

    public int removeClient(int id) {
        Connection connection = manager.getConnection();
        if(clientLinkedToFile(id, connection) == 0){
            try {
                delete = connection.prepareStatement(
                        "DELETE  "
                                + "FROM opdrachtgever WHERE ogever_id = ? ");
                delete.setInt(1, id);
                delete.executeUpdate();
                }  
             catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.close();
                    
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return 1;
        } else {
            try{
                connection.close();
                return 0;
            } catch(SQLException e){
                
            }
            
        }
        return 0;
    }


    public Opdrachtgever getClients(int id) {
        Connection connection = manager.getConnection();
        Opdrachtgever opdrachtgever = new Opdrachtgever();
        ResultSet resultSet = null;
        try {
            select = connection.prepareStatement(
                    "SELECT ogever_id, ogever_naam, ogever_telefoonnr, "
                            + "ogever_contactpersoon, ogever_contactpersoon_telnr"
                            + " FROM opdrachtgever WHERE ogever_id = ?;");
            select.setInt(1, id);
            resultSet = select.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (resultSet != null) {
                while (resultSet.next()) {
                    opdrachtgever = new Opdrachtgever(
                            resultSet.getInt("ogever_id"),
                            resultSet.getString("ogever_naam"),
                            resultSet.getString("ogever_telefoonnr"),
                            resultSet.getString("ogever_contactpersoon"),
                            resultSet.getString("ogever_contactpersoon_telnr")
                    );
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                //
            }
        }
        return opdrachtgever;
    }

    public void updateClient(Opdrachtgever opdrachtgever) {
        Connection connection = manager.getConnection();
        try {
            update = connection.prepareStatement(
                    "UPDATE opdrachtgever SET "
                            + "ogever_naam=?,"
                            + "ogever_contactpersoon=?,"
                            + "ogever_telefoonnr=? WHERE ogever_id=?");
            update.setString(1, opdrachtgever.getNaam());
            update.setString(2, opdrachtgever.getContactpersoonnaam());
            update.setString(3, opdrachtgever.getTelefoonnummer());
            update.setInt(4, opdrachtgever.getId());
            update.executeUpdate();
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            try {
                update.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateSchade(SoortSchade soortschade) {
        Connection connection = manager.getConnection();
        try {
            update = connection.prepareStatement(
                    "UPDATE soort_schade SET "
                            + "schade_naam=? WHERE schade_id=?");
            update.setString(1, soortschade.getNaam());
            update.setInt(2, soortschade.getId());
            update.executeUpdate();
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public int removeSoortSchade(int id) {
        Connection connection = manager.getConnection();
        if(damageLinkedToFile(id, connection) == 0){
            try {

                    delete = connection.prepareStatement(
                            "DELETE from soort_schade  "
                                    + "WHERE schade_id=?");
                    delete.setInt(1, id);
                    delete.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return 1;
        } else {
            try{
                connection.close();
                return 0;
            } catch(SQLException e){
                
            }
        }
        return 0;
    }

    private int clientLinkedToFile(int id, Connection connection) {
        ResultSet results = null;
        try {
            select = connection.prepareStatement(
                    "SELECT  "
                            + "FROM dossier WHERE dossier_ogever_id = ? ");
            select.setInt(1, id);
            results = select.executeQuery();
            if(results.next()){
                return 1;
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    private int damageLinkedToFile(int id, Connection connection) {
        ResultSet results = null;
        try {
            select = connection.prepareStatement(
                    "SELECT  "
                            + "FROM dossier WHERE dossier_soortschade_id = ? ");
            select.setInt(1, id);
            results = select.executeQuery();
            if(results.next()){
              return 1;  
            } else {
              return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
